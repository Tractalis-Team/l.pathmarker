(function (factory, window) {
    if (typeof define === 'function' && define.amd) {
        define(['leaflet'], factory);
    } else if (typeof exports === 'object') {
        module.exports = factory(require('leaflet'));
    }
    if (typeof window !== 'undefined' && window.L) {
        window.L.PathMarker = factory(L);
    }
}(function (L) {
    var Animator = {
        _animatedMarkers: {},
        _animationId: 0,
        _count: 0,

        run: function (marker, latLng, animLength) {
            this._animationId = this._animationId || requestAnimationFrame(this._animStep.bind(this));
            let oldLatLon = marker.getLatLng();
            let id = L.Util.stamp(marker);

            if (!this._animatedMarkers[id]) {
                this._count++;
            }

            this._animatedMarkers[id] = {
                id: id,
                marker: marker,
                from: oldLatLon,
                to: latLng,
                diff: L.latLng(latLng.lat - oldLatLon.lat, latLng.lng - oldLatLon.lng),
                animationLength: animLength || 300
            };
        },

        _animStep: function (ts) {
            let ids = Object.getOwnPropertyNames(this._animatedMarkers);
            if (this._count > 0) {
                this._animationId = window.requestAnimationFrame(this._animStep.bind(this));
            }

            for (let i = 0; i < ids.length; i++) {
                let anim = this._animatedMarkers[ids[i]];

                anim.start = anim.start || ts;
                let progress = (ts - anim.start) / anim.animationLength;
                let marker = anim.marker;

                if (progress < 1) {
                    marker._latlng = L.latLng(
                        anim.from.lat + (anim.diff.lat * progress),
                        anim.from.lng + (anim.diff.lng * progress)
                    );

                    marker._point = marker._map.latLngToLayerPoint(marker._latlng);
                    L.DomUtil.setTransform(marker._path, marker._point, marker._scale);
                } else {
                    marker.setLatLng(anim.to);
                    this.stop(anim.id);
                }
            }
        },

        stop: function (id) {
            if (this._animatedMarkers[id]) {
                delete this._animatedMarkers[id];
                this._count--;

                if(this._count === 0) {
                    cancelAnimationFrame(this._animationId);
                    this._animationId = 0;
                }
            }
        }
    };

    var PathMarker = L.Path.extend({
        options: {
            fill: true,
            scale: 1,
            radius: 9,
            // ~9px outer radius, 5 arm star
            path: 'm8-9 2.2 6.4h6.8l-5.2 4.1 1.8 6.4-5.6-3.8-5.6 3.8 1.8-6.4-5.2-4.1h6.8z',
            animationLength: 300
        },

        initialize: function (latlng, options) {
            L.Util.setOptions(this, options);
            this._latlng = L.latLng(latlng);
            this._radius = this.options.radius;
            this._scale = this.options.scale;
            this._pathString = this.options.path;
        },

        setLatLng: function (latLng, animate) {
            if (animate) {
                Animator.run(this, latLng, this.options.animationLength);
                return this;
            }

            this._latlng = L.latLng(latLng);
            this.redraw();

            this.fire('move', {
                latlng: this._latlng
            });

            return this;
        },

        getLatLng: function () {
            return this._latlng;
        },

        getRadius: function () {
            return this._radius;
        },

        getScale: function () {
            return this.scale;
        },

        setScale: function (scale) {
            this._scale = scale;
            this._radius = this.options.radius * scale; 
            this._path && L.DomUtil.setTransform(this._path, this._point, this._scale);
        },

        setStyle: function (options) {
            L.Path.prototype.setStyle.call(this, options);
            return this;
        },

        _project: function () {
            this._point = this._map.latLngToLayerPoint(this._latlng);
            this._updateBounds();
        },

        _updateBounds: function () {
            var r = this._radius,
                w = this._clickTolerance(),
                p = [r + w, r + w];
            this._pxBounds = L.bounds(this._point.subtract(p), this._point.add(p));
        },

        _update: function () {
            if (this._map) {
                this._updatePath();
            }
        },

        _updatePath: function () {
            const p = this._point;
            const d = this._empty() ? 'M0 0' : this._pathString;

            if (!this._path) return;

            if (this._path.getAttribute('d') !== d) {
                this._renderer._setPath(this, d);
            }

            L.DomUtil.setTransform(this._path, p, this._scale);
        },

        _empty: function () {
            return this._radius && !this._renderer._bounds.intersects(this._pxBounds);
        },

        _containsPoint: function (p) {
            return p.distanceTo(this._point) <= this._radius + this._clickTolerance();
        }
    });

    L.pathMarker = function (latlng, options) {
        return new PathMarker(latlng, options);
    }

    return PathMarker;
}, window));